package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.StockWithTimestamp;
import com.example.demo.entities.Stocks;
import com.example.demo.repository.StocksRepository;

@Service
public class StocksService {
	@Autowired
	private StocksRepository repository;

	public List<Stocks> getAllStocks() {
		return repository.getAllStocks();
	}

	public Stocks getStocks(int id) {
		return repository.getStocksById(id);
	}

	public Stocks saveStocks(Stocks Stocks) {
		return repository.editStocks(Stocks);
	}

	public Stocks newStocks(Stocks Stocks) {
		return repository.addStocks(Stocks);
	}

	public int deleteStocks(int id) {
		return repository.deleteStocks(id);
	}
}